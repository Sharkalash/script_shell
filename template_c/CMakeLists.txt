cmake_minimum_required(VERSION 2.6)

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  set(CMAKE_C_FLAGS "-Wall -Wextra -g ${CMAKE_CXX_FLAGS}")
endif()

include_directories(src)

file(GLOB_RECURSE ${PROJECT_NAME}_sources src/${PROJECT_NAME}/*.c)
add_executable( ${PROJECT_NAME} ${${PROJECT_NAME}_sources})

file(GLOB_RECURSE testing_sources src/testing/*.c)
add_executable( testing ${testing_sources} )
