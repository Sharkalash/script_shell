#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <stdlib.h>

#define WIDTH 800
#define HEIGHT 800
#define NAME "Super Game"
#define GAME 0

void event_manager(int (*statement)(float))
{
  int   tps, ticks, oldTps = 0;
  float dt;
  int   run = 1;
  int * dataw;
  
  while(run)
    {
      tps = SDL_GetTicks();
      dt = tps - oldTps;
      oldTps = tps;
      
      run = !PollEvent(NULL);
      
      getDataWindow((void **)&dataw);
      
      run = run && dataw;
      dataw = (run)?dataw:0;

      if(statement)
	run = run && statement(dt);
      
      updateWindow();
      displayWindow();
      ticks = 16 - SDL_GetTicks() + tps;

      if(ticks > 0)
	SDL_Delay(ticks);

      if(initIteratorWindow())
	run = 0;
      
    }
}

int main()
{
  int black[4] = {0,0,0,0};
  int dataw = 1;
 
  if(initAllSANDAL2(IMG_INIT_JPG))
    {
      puts("Failed to init SANDAL2");
      return EXIT_FAILURE;
    }

  createWindow(WIDTH, HEIGHT, NAME, 0, black, GAME);
  
  if(initIteratorWindow())
    {
      closeAllSANDAL2();
      fprintf(stderr, "Failed to open window \n");
      return EXIT_FAILURE;
    }

  setDataWindow(&dataw);

  event_manager(NULL);
 
  return EXIT_SUCCESS;
}
